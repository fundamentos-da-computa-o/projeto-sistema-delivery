[Início]
  |
  V
[Usuário Inicia a Aplicação]
  |
  V
[Visualizar Lista de Produtos] -->[Selecionar Produto]-->[Ver Detalhes do Produto]
  |               |                       |                        |
  V               V                       |                        |
[Adicionar Produto ao Carrinho] <------- |                        |
  |               |                       |                        |
  V               V                       |                        |
[Carrinho de Compras]-------------------- |                        |
  |               |                                             |
  V               V                                             |
[Finalizar Pedido] -------------------------------------------  |
  |               |                                             |
  V               V                                             |
[Enviar Detalhes do Pedido via WhatsApp] ------------------------|
  |               |
  V               |
[Terminar]
