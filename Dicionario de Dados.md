**Dicionário de Dados: Sistema de Pedidos via Delivery**

1. **Usuário**
   - Descrição: Representa os usuários que utilizam o sistema.
   - Atributos:
     - ID (identificador único do usuário)
     - Nome (nome do usuário)
     - Telefone (número de telefone do usuário)
     - Endereço (endereço de entrega do usuário)

2. **Produto**
   - Descrição: Representa os produtos disponíveis para pedido.
   - Atributos:
     - ID (identificador único do produto)
     - Nome (nome do produto)
     - Descrição (descrição do produto)
     - Preço (preço do produto)

3. **Carrinho de Compras**
   - Descrição: Representa o carrinho de compras de um usuário, que contém os produtos selecionados.
   - Atributos:
     - ID (identificador único do carrinho)
     - Produtos (lista de produtos no carrinho)

4. **Item do Carrinho**
   - Descrição: Representa um item no carrinho de compras de um usuário.
   - Atributos:
     - ID (identificador único do item)
     - Produto (referência ao produto no carrinho)
     - Quantidade (quantidade do produto no carrinho)

5. **Pedido**
   - Descrição: Representa um pedido feito por um usuário.
   - Atributos:
     - ID (identificador único do pedido)
     - Usuário (referência ao usuário que fez o pedido)
     - Carrinho (referência ao carrinho de compras associado ao pedido)
     - Status (status do pedido, como "pendente", "entregue", etc.)
     - Endereço de Entrega (endereço de entrega para o pedido)

